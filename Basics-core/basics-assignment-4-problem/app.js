const app = Vue.createApp({
  data() {
    return {
      fisrtInput: "",
      isShow: true,
      colorValue: '',
    };
  },
  computed: {
    visibility() {
      if (this.isShow) {
        return { visible: true };
      } else return { hidden: true };
    },
  },
  methods: {
    whenShow() {
      this.isShow = !this.isShow;
      console.log("show: ", this.isShow);
    },
  },
});

app.mount("#assignment");
