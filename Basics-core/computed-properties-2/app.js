const app = Vue.createApp({
    data () {
        return {
            counter: 0,
            name: ''
        };
    },
    computed: {
        //donnee calculer ou conbiner a etre uniquement afficher
        fullname() {
            console.log("Running again...");
            if(this.name === ''){
                return '';
            }
            return this.name + ' ' + 'Yvon';
        }
    },
    methods: {
        outputFullname() {
            console.log("Running again...");
            if(this.name === ''){
                return '';
            }
            return this.name + ' ' + 'Yvon';
        },
        resetInput(){
            this.name = '';
        },
        IncrementCount(){
            this.counter = this.counter + 10;
        },
        decrementCount() {
           this.counter =  this.counter - 5;
        }
    }
});

app.mount("#assignment")