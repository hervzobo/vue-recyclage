const app = Vue.createApp({
	data() {
		return {
			text: 'Hello',
			showText: ''
		};
	},
	methods: {
		showAlert() {
			alert("Hello Yvon");
		},
		saveValue(event) {
			this.text = event.target.value;
		},
		confirmedValue() {
			this.showText = this.text;
		}
	}
});


app.mount("#assignment")