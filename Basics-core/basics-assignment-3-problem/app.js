const app = Vue.createApp({
    data() {
        return {
            result: 0,
            message: '',
        };
    },
    watch: {
        result(){
            const that = this;
            setTimeout(() => {
                that.result = 0;
            }, 5000)
        }
    },
    computed: {
        showMessage(){
            if(this.result == 37){
                this.message = this.result + ' Not there yet';
            }else if(this.result > 37){
                this.message = this.result + ' Too much!';
            }else{
                this.message = this.result;
            }
            return this.message;
        }
    },
    methods: {
        add5(){
            this.result+=5;
        },
        add1(){
            this.result+=1;
        }
    }
});

app.mount("#assignment");