const app = Vue.createApp({
	data() {
		return {
			myName: 'Yvon ZOBO',
			myAge: 27,
			vueImage: 'https://media.istockphoto.com/photos/modern-digital-marketing-on-the-tablet-in-vintage-style-picture-id615253358',
		};
	},
	methods: {
		favoriteNumber() {
			const randomNumber = Math.random();
			if(randomNumber > 0 && randomNumber <= 1){
				return randomNumber;	
			}
			return 0;
		},
		recalculateAge() {
			return this.myAge+5;
		}
	}
});
app.mount('#assignment');