const app = Vue.createApp({
    data () {
        return {
            counter: 0,
            name: '',
            fullname: ''
        };
    },
    watch: {
        counter(value) {
            if(value > 50){
                const that = this;
                setTimeout(() => {
                    that.counter = 0;
                }, 2000);
            }
        },
        /**
         * This tells Vue one important thing.
         * Whenever name changes, this watcher method will re-execute
         * @param {*} value 
         */
        name(value) {
            if(value === ''){
                this.fullname = '';
            }else {
                this.fullname = value + ' ' + 'Yvon';
            }
        }
    },
    computed: {
        // //donnee calculer ou conbiner a etre uniquement afficher
        // fullname() {
        //     console.log("Running again...");
        //     if(this.name === ''){
        //         return '';
        //     }
        //     return this.name + ' ' + 'Yvon';
        // }
    },
    methods: {
        outputFullname() {
            console.log("Running again...");
            if(this.name === ''){
                return '';
            }
            return this.name + ' ' + 'Yvon';
        },
        resetInput(){
            this.name = '';
        },
        IncrementCount(){
            this.counter = this.counter + 10;
        },
        decrementCount() {
           this.counter =  this.counter - 5;
        }
    }
});

app.mount("#assignment")