const app = Vue.createApp({
    data() {
        return {
            tasks: [],
            task: '',
            isVisible: false,
        };
    },
    computed: {
        visibilityTaskList() {
            if (this.tasks.length === 0)
                return 'Hide / Show List';
            else if (this.isVisible) {
                return 'Hide List';
            } else {
                return 'Show List';
            }
            
        }
    },
    methods: {
        addTask() {
            this.tasks.push(this.task);
        },
        showTasks() {
            this.isVisible = !this.isVisible;
        }
    },
});

app.mount('#assignment');