# Module Content

- Rendering Content with Conditions
- Outputing Lists of Data
- A first Look Behind the Scenes

## Summary


### Conditional Content

- **v-if** (and **v-show**) allows you to render content **only if a certain condition is met**
- v-if can be combined with **v-else** and **v-else-if** (only on direct sibling elements!)

### v-for Variations

- You can extract **values**, values and **indexes** or values, **keys** and indexes
- If you need v-for and v-if, **Don't use them on the same elelement**. Use a wrapper with v-if instead.

#### List

- v-for can be used to render multiple elelments dynamically
- v-for can be used with arrays, objects and ranges (numbers).

#### Keys

- Vue **re-uses DOM elements** to optimize performance -> This can lead to bugs if elelments contain state.
- Bind the **Key** attribute to a unique value to help Vue identify elements that belong to list content.